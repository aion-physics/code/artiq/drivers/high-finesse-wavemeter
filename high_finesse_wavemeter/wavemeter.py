import logging
import os
import sys

from high_finesse_wavemeter import wlmData

# HighFinesse API imports

logger = logging.getLogger(__name__)


class WavemeterError(RuntimeError):
    pass


DLL_PATH_DEFAULT = r"c:\windows\system32\wlmData.dll"

DLL_PATH = os.getenv("WAVEMETER_DLL_PATH")
if DLL_PATH is None:
    logger.warning(
        "WAVEMETER_DLL_PATH environment variable not set: defaulting to %s",
        DLL_PATH_DEFAULT,
    )
    DLL_PATH = DLL_PATH_DEFAULT
else:
    logger.info("WAVEMETER_DLL_PATH={}".format(DLL_PATH))


_dll_loaded = False

def load_dll():
    global _dll_loaded
    if not _dll_loaded:
        try:
            wlmData.LoadDLL(DLL_PATH)
            _dll_loaded=True
        except:
            logger.error(
                "Error: Could not find the DLL for the High Finesse wavemeter. Please check the WAVEMETER_DLL_PATH environment variable."
            )
            sys.exit(1)


class Wavemeter:
    def __enter__(self):
        load_dll()
        if wlmData.dll.GetWLMCount(0) == 0:
            raise WavemeterError("There is no running wlmServer instance(s).")

    def __exit__(self):
        pass

    def get_wavelength(self):
        """Get the wavelength on channel 2.

        This is hard-coded for historical reasons: you should use
        get_wavelength_channel instead.

        Returns:
            float: The wavelength in nm.

        Raises:
            WavemeterError: Raised if the measurement fails for some reason.
        """
        wavelength = wlmData.dll.GetWavelengthNum(2, 0.0)
        if wavelength <= 0:
            raise WavemeterError("get_wavelength returned error code: %d" % wavelength)
        return wavelength

    def get_wavelength_channel(self, channel: int):
        """Get the wavelength from a particular channel of the wavemeter

        Args:
            channel (int): The channel to read, from 1 to 8.

        Returns:
            float: The wavelength in nm.

        Raises:
            WavemeterError: Raised if the measurement fails for some reason.
        """
        wavelength = wlmData.dll.GetWavelengthNum(channel, 0.0)
        if wavelength <= 0:
            raise WavemeterError("get_wavelength returned error code: %d" % wavelength)
        return wavelength
