import argparse
import asyncio
import logging
import sys

from sipyco import common_args
from sipyco.pc_rpc import Server

from high_finesse_wavemeter.wavemeter import Wavemeter

logging.basicConfig()

logger = logging.getLogger(__name__)


def get_argparser():
    parser = argparse.ArgumentParser(
        description="""NDSP controller for the high finesse wavemeter."""
    )
    common_args.simple_network_args(parser, 3272)
    common_args.verbosity_args(parser)
    return parser


def main():
    logger.info("Starting wavemeter NDSP server")
    args = get_argparser().parse_args()
    common_args.init_logger_from_args(args)

    async def run():
        server = Server({"wavemeter": Wavemeter()}, None, True)
        await server.start(common_args.bind_address_from_args(args), args.port)
        try:
            await server.wait_terminate()
        finally:
            await server.stop()

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run())
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()


if __name__ == "__main__":
    main()
