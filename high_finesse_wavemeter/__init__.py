"""high_finesse_wavemeter - Exposes a USB connected High Finesse Wavemeter as a NDSP for artiq."""

__author__ = "Elliot Bentine <elliot.bentine@physics.ox.ac.uk>"
__all__ = []

from ._version import get_version

__version__ = get_version()
del get_version
