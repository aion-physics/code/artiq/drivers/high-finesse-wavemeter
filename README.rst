High Finesse Wavemeter
======================

**Exposes a USB connected High Finesse Wavemeter as a NDSP for artiq.**

*Elliot Bentine 2022*

Installation and running on Windows
-----------------------------------

1. Install `conda`
2. `conda config --prepend channels https://conda.m-labs.hk/artiq`
3. `conda create --name wavemeter sipyco`
4. Configure the `WAVEMETER_DLL_PATH` environment variable to point to your
   wavemeter dll, e.g. `setx WAVEMETER_DLL_PATH Path/to/my/dll`. For example,
   `C:\Windows\System32\wlmData.dll`.

To run the program:

1. Open a prompt in directory and `conda activate wavemeter`.
2. `python -m high_finesse_wavemeter.run_wavemeter --bind 0.0.0.0`.

Development
-----------

This package was generated automatically by an opinionated `cookiecutter
<https://github.com/audreyr/cookiecutter>`_ template: `PyPackage
<https://gitlab.com/aion-physics/code/pypackage-template>`_. See this package
for documentation about various features implemented here.


Authors
-------

`high_finesse_wavemeter` was written by `Elliot Bentine <elliot.bentine@physics.ox.ac.uk>`_.

The `pypackage template <https://gitlab.com/aion-physics/code/pypackage-template>`_ from which this package was generated was written by Charles Baynham and inspired by `cookiecutter-pypackage-minimal <https://github.com/kragniz/cookiecutter-pypackage-minimal>`_
